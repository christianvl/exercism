  }

  test("shouting with special characters") {
    pending
    Bob.response("ZOMG THE %^*@#$(*^ ZOMBIES ARE COMING!!11!!1!") should be(
      "Whoa, chill out!")
  }

  test("shouting with no exclamation mark") {
    pending
    Bob.response("I HATE THE DMV") should be("Whoa, chill out!")
  }

  test("statement containing question mark") {
    pending
    Bob.response("Ending with ? means a question.") should be("Whatever.")
  }

  test("non-letters with question") {
    pending
    Bob.response(":) ?") should be("Sure.")
  }

  test("prattling on") {
    pending
    Bob.response("Wait! Hang on. Are you going to be OK?") should be("Sure.")
  }

  test("silence") {
    pending
    Bob.response("") should be("Fine. Be that way!")
  }

  test("prolonged silence") {
    pending
    Bob.response("          ") should be("Fine. Be that way!")
  }

  test("alternate silence") {
    pending
    Bob.response("										") should be("Fine. Be that way!")
  }

  test("multiple line question") {
    pending
    Bob.response("""Does this cryogenic chamber make me look fat?
No.""") should be("Whatever.")
  }

  test("starting with whitespace") {
    pending
    Bob.response("         hmmmmmmm...") should be("Whatever.")
  }

  test("ending with whitespace") {
    pending
    Bob.response("Okay if like my  spacebar  quite a bit?   ") should be(
      "Sure.")
  }

  test("other whitespace") {
    pending
    Bob.response("""
                  """) should be("Fine. Be that way!")
  }
  test("non-question ending with whitespace") {
    pending
    Bob.response("This is a statement ending with whitespace      ") should be(
      "Whatever.")
  }
}
