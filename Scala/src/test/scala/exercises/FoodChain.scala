object FoodChain {

  val subjects: Seq[String] = Seq("fly", "spider", "bird", "cat", "dog", "goat", "cow", "horse")

  val actions: Map[Int, String] = Map(1 -> "It wriggled and jiggled and tickled inside her.",
                                 2 -> "How absurd to swallow a bird!", 3 -> "Imagine that, to swallow a cat!",
                                 4 -> "What a hog, to swallow a dog!",
                                 5 -> "Just opened her throat and swallowed a goat!",
                                 6 -> "I don't know how she swallowed a cow!")

  def firstVerse(subject: String): String = {
    s"I know an old lady who swallowed a $subject.\n"
  }

  def lastVerse(subject: String): String = {
    val last = subject match {
      case horse if subject.equals("horse") => "She's dead, of course!\n\n"
      case _ => s"I don't know why she swallowed the fly. Perhaps she'll die.\n\n"
    }
    last
  }

  def intermediateVerses(n: Int): String = {
    List(s"She swallowed the ${subjects(n)} to catch the ${subjects(n-1)}",
    n match {
      case 2 => " that wriggled and jiggled and tickled inside her."
      case _ => "."
    }, "\n").mkString
  }

  def stanza(n: Int): String = {
    val lyrics = {
      List(firstVerse(subjects(n)),
      n match {
        case 0 => ""
        case 7 => ""
        case _ => {
          List(s"${actions.getOrElse(n, n)}\n",
          (1 to n).reverse.map(x => intermediateVerses(x)).mkString).mkString
        }
      },
      s"${lastVerse(subjects(n))}")
    }
    lyrics.mkString
  }

  def recite(from: Int, to: Int): String = {
    (from-1 to to-1).map(n => stanza(n)).mkString
  }

}

