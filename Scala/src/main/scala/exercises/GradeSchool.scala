import scala.collection.mutable.Map

class School {
  type DB = Map[Int, Seq[String]]
  
  def add(name: String, g: Int) = {
    db = db.clone.addOne(g, grade(g) :+ name)
  }

  var db: DB = Map.empty
  
  def grade(g: Int): Seq[String] = {
    db.getOrElse(g, Seq())
  }

  def sorted: DB = db.map(x => (x._1, x._2.sorted))
}
