object Bob {
  def response(statement: String): String = {
    val alphabet = "ABCDEFGHIJKLMNOPQRSTUVXWYZ".toList
    statement match {
      case e if e.trim.isEmpty                                                          => "Fine. Be that way!"
      case q if q.trim.last == '?' => q match {
        case lower if lower != lower.toUpperCase || 
        lower.toUpperCase.forall(c => !alphabet.contains(c))                            => "Sure."
        case upper if upper == upper.toUpperCase                                        => "Calm down, I know what I'm doing!"
      }
      case y if y == y.toUpperCase && !y.toUpperCase.forall(c => !alphabet.contains(c)) => "Whoa, chill out!"
      case _                                                                            => "Whatever."
    }
  }
}
