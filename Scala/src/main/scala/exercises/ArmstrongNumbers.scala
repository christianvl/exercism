object ArmstrongNumbers {

  def isArmstrongNumber(x: Int): Boolean = {
    val numbers = x.toString().toList.map(c => c.asDigit)
    val power = numbers.size
    val y = numbers.foldLeft(0)((acc, n) => scala.math.pow(n, power).toInt + acc)
    y == x
  }
  
}
