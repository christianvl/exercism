object Isogram {

  def isIsogram(candidate: String): Boolean = {
    val clean: String = candidate.filter(_.isLetter).toLowerCase
    clean.size == clean.toSet.size
  }
}
