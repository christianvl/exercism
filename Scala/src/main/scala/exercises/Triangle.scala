case class Triangle(a: Double, b: Double, c: Double) {

  val sides = Set(a,b,c)

  def isTriangle: Boolean = {
    sides match {
      case no if no.contains(0.0)                     => false
      case no if a + b < c || a + c < b || c + b < a  => false
      case _                                          => true
    }
  }

  def equilateral: Boolean = {
    this.isTriangle && sides.size == 1
  }
  def isosceles: Boolean = {
    this.isTriangle && sides.size <= 2
  }
  def scalene: Boolean = {
    this.isTriangle && sides.size == 3
  }
}
