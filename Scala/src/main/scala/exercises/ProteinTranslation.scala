object ProteinTranslation {

  def rnaToProtein(rna: String): String = {
    rna match {
      case a if Seq("AUG").contains(rna) => "Methionine"
      case b if Seq("UUU", "UUC").contains(rna) => "Phenylalanine"
      case c if Seq("UUA", "UUG").contains(rna) => "Leucine"
      case d if Seq("UCU", "UCC", "UCA", "UCG").contains(rna) => "Serine"
      case e if Seq("UAU", "UAC").contains(rna) => "Tyrosine"
      case f if Seq("UGU", "UGC").contains(rna) => "Cysteine"
      case g if Seq("UGG").contains(rna) => "Tryptophan"
      case h if Seq("UAA", "UAG", "UGA").contains(rna) => "STOP"
      case _ => "Invalid"
    }
  }

  def proteins(rna: String): Seq[String] = {
    rna.grouped(3).map(rnaToProtein).takeWhile(_ != "STOP").toList
  }

}
