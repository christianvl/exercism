import java.time.{Instant, LocalDate, LocalDateTime, ZonedDateTime}

object Gigasecond {

  def add(startDate: LocalDate): LocalDateTime = {
    add(startDate.atStartOfDay)
  }

  def add(startDateTime: LocalDateTime): LocalDateTime = {
    val gigas = (startDateTime.toEpochSecond(ZonedDateTime.now().getOffset()) + 1e9).toLong
    Instant.ofEpochSecond(gigas).atZone(ZonedDateTime.now().getOffset()).toLocalDateTime
  }

}
