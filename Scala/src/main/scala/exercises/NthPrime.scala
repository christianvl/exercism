object NthPrime {

  def prime(n: Int): Option[Int] = {
    n match {
      case 0 => None
      case _ => Option(buildPrimes(n).last)
    }
  }

  def isPrime(n: Int): Boolean = {
    // on its own, this function should return false if n <= 1
    // but, because it is only called with n >= 2, there's no need to test if n <= 1 for each call
    !(2 to Math.sqrt(n).toInt).exists(x => n % x == 0)
  }

  def buildPrimes(size: Int, curr: Int = 2, acc: Seq[Int] = Seq[Int]()): Seq[Int] = {
    size match {
      case _ if size == acc.size => acc
      case _ if isPrime(curr) => buildPrimes(size, curr +1, acc.:+(curr))
      case _ => buildPrimes(size, curr +1, acc)
    }
  }

}

object NthPrime2 {

  val primes: Stream[Int] = 2 #:: Stream.from(3, 2).filter(isPrime)

  def isPrime(number: Int): Boolean =
    primes.takeWhile(_ <= math.sqrt(number)).forall(number % _ != 0)

  def prime(n: Int): Option[Int] = n match {
    case 0 => None
    case _ => Some(primes.take(n).last)
  }

}
