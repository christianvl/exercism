import scala.math.pow

object Grains {

  def square(n: Int): Option[Int|BigInt] = {
    n match {
      case negative if n <= 0 => None
      case over if n >= 65 => None
      case _ => Some(pow(2, n -1).round)
    }
  }

  val total: BigInt = BigInt((1) << 64) - 1

}
