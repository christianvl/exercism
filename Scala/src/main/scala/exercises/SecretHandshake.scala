object SecretHandshake {

  // val input = 2

  val actions = List("wink", "double blink", "close your eyes", "jump")

  def commands(input: Int): List[String] = {
    val bin = input.toBinaryString

    val y = bin.length match {
      case x if x > 4 => bin.tail.reverse.zip(0 to bin.length -1)
      case _ => bin.reverse.zip(0 to bin.length -1)
    }

    val z = y.filter(_._1 == '1').map { case (b,i) => actions(i) }.toList

    val reverse =
      bin.charAt(0) match {
        case a if (a > 4) && (a == '1') => z.reverse
        case _ => z
      }
    reverse
  }
}

SecretHandshake.commands(2)
