object PrimeFactors {

  def factors(n: Long): List[Long] = {
  n match {
    case invalid if n <= 1 => List.empty
    case _ => div(n)
  }
}

  private def div(dividend: Long, divisor: Long = 2, acc: List[Long] = List.empty): List[Long] = {
    dividend match {
      case done if dividend / divisor == 1  => acc :+ dividend
      case a if (dividend % divisor == 0)   => div(dividend / divisor, divisor, acc :+ divisor)
      case _                                => div(dividend, divisor + 1, acc)
    }
  }

}
