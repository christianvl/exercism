object Darts {

  def score(x: Float, y: Float): Int = {
    scala.math.hypot(x, y) match {
      case bullseye if bullseye <= 1 => 10
      case mid if mid <= 5           => 5
      case larger if larger <= 10    => 1
      case _                         => 0
    }
  }

}
