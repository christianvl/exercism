object ScrabbleScore {

  val points = List(
    (List('A', 'E', 'I', 'O', 'U', 'L', 'N', 'R', 'S', 'T'), 1),
    (List('D', 'G'), 2),
    (List('B', 'C', 'M', 'P'), 3),
    (List('F', 'H', 'V', 'M', 'Y'), 4),
    (List('K'), 5),
    (List('X', 'J'), 8),
    (List('Q', 'Z'), 10))


    def score(chars: String): Int = {
    val norm = chars.toUpperCase()
    norm.flatMap(ch => points.map(xs => { xs._1.count(_ == ch) * xs._2})).sum
  }

}
