import scala.util.Random
case class Robot(chars: Int = 2, digits: Int = 3) {

  def randomChars(thisMany: Int): String = {
    val str = for {
      i <- 1 to thisMany
    } yield Random.alphanumeric.filter(_.isLetter).head.toString().toUpperCase()
    str.mkString
  }

  def randomDigits(thisMany: Int): String = {
    val str = for {
      i <- 1 to thisMany
    } yield Random.nextInt(10)
    str.mkString
  }

  var name = randomChars(chars) + randomDigits(digits)

  def reset() = {
    this.name = randomChars(chars) + randomDigits(digits)
  }

}
