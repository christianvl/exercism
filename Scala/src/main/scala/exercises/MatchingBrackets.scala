object MatchingBrackets {
  def isPaired(brackets: String): Boolean = {

    val BrMap = Map('{' -> '}', '[' -> ']', '(' -> ')', '}' -> '{', ']' -> '[', ')' -> '(')
    val openB = List('{', '[', '(')
    val closeB = List('}', ']', ')')

    def cb(str: String, previous: List[Char] = List.empty): Int = {
      str match {
        case blank if str.isEmpty => previous.size
        case unb if previous.size == 0 && closeB.contains(str.charAt(0)) => 1
        case add if openB.contains(str.charAt(0)) => cb(str.tail, previous :+ str.charAt(0))
        case remove if previous.size > 0 && BrMap.getOrElse(str.charAt(0), "!") == previous.last => cb(str.tail, previous.dropRight(1))
        case _ => previous.size
      }
    }

    val x = brackets.filter(BrMap.contains(_))
    cb(x) == 0
  }
}
