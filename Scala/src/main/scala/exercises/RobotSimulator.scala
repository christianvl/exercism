object Bearing extends Enumeration {
  type Bearing = Value
  val North, East, South, West = Value

  val toRightOf = Map(North -> East, East -> South, South -> West, West -> North)
  val toLeftOf = toRightOf.map { case (k, v) => (v, k) }
}

import Bearing.Bearing

case class Robot(bearing: Bearing, coordinates: (Int, Int)) {

  def advance: Robot = bearing match {
    case Bearing.South => Robot(bearing, (coordinates._1, coordinates._2-1))
    case Bearing.North => Robot(bearing, (coordinates._1, coordinates._2+1))
    case Bearing.East => Robot(bearing, (coordinates._1+1, coordinates._2))
    case Bearing.West => Robot(bearing, (coordinates._1-1, coordinates._2))
  }

  def turnRight: Robot = Robot(Bearing.toRightOf(bearing), coordinates)

  def turnLeft: Robot = Robot(Bearing.toLeftOf(bearing), coordinates)

  def simulate(movements: String): Robot = {
    movements.foldLeft(this) {
      case (robot, move) => move match {
        case 'A' => robot.advance
        case 'R' => robot.turnRight
        case 'L' => robot.turnLeft
      }
    }
  }
}
