object FlattenArray {

  def flatter(l: List[Any]): List[Any] = l.flatMap {
    case null => None
    case sublist: List[_] => flatter(sublist)
    case element => List(element)
  }

}
