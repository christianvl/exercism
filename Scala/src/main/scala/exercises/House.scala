object House {

  val subjects: Seq[String] = Seq("house", "malt", "rat", "cat", "dog", "cow with the crumpled horn",
                                 "maiden all forlorn", "man all tattered and torn",
                                 "priest all shaven and shorn", "rooster that crowed in the morn",
                                 "farmer sowing his corn", "horse and the hound and the horn")

//                               that

  val actions: Seq[String] = Seq("Jack built.", "lay in", "ate", "killed", "worried", "tossed",
                               "milked", "kissed", "married", "woke", "kept", "belonged to")

  val verses: Map[Int, String] = {
  (1 to 12).map(i => i -> s"the ${subjects(i - 1)} that ${actions(i - 1)} ").toMap
  }

  def reciteOne(firstVerse: Int): String = {
    val music = {
      (1 to firstVerse).foldRight(List[String]()) { (i, song) =>
        val verse = verses.getOrElse(i, "")
        song :+ verse
      }
    }
    s"This is ${music.mkString.trim()}"
  }

  def recite(first: Int, repetitions: Int): String = {
    val music = {
      (first to repetitions).foldLeft(List[String]()) { (song, i) =>
        val verse = reciteOne(i)
        i match {
          case a if i == repetitions  => song :+ verse :+ "\n\n"
          case _                      => song :+ verse :+ "\n"
        }
      }
    }
    music.mkString
  }

}
