case class WordCount(str: String) {

  def countWords: Map[String, Int] = {
    val norm = str
      .toLowerCase()
      .replace('\n', ' ')
      .replace(',', ' ')
      .replaceAll(" '", " ").replaceAll("' ", " ")
      .filter(c =>  { c >= 'a' && c <= 'z' ||
                       c == ' ' || c == '\'' ||
                       c.isDigit })
      .split(" ")
      .filterNot(_.isEmpty)

    norm.foldLeft(Map.empty: Map[String, Int])((m, word) => m.updated(word, m.getOrElse(word, 0) + 1))
  }

}
