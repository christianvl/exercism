object PascalsTriangle {

  def rows(rowsT: Int, currRow: Int = 1, acc: List[List[Int]] = List.empty): List[List[Int]] = {
    rowsT match {
      case 0 => acc
      case done if currRow > rowsT => acc
      case _ => {
        val row = {
          currRow match {
            case 1 => List(1)
            case 2 => List(1, 1)
            case 3 => List(1, 2, 1)
            case _ => {
              val t =
                for (i <- 0 until acc.last.size -2)
                yield (acc.last(i) + acc.last(i+1))
              t.toList.::(1) :+ t.head :+ 1
            }
          }
        }
        rows(rowsT, currRow + 1, acc :+ row)
      }
    }
  }

  //alternative
  def rows2(count: Int): Seq[Seq[Int]] =
    Vector.iterate(Vector(1), count) {
      row => 0 +: row zip row :+ 0 map Function.tupled(_ + _)
    }

}
