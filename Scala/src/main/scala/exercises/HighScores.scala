object HighScores {

  def latest(scores: List[Int]): Int = scores.last

  def personalTop(scores: List[Int]): List[Int] = {
    scores.sorted(Ordering[Int].reverse).take(3)
  }

  def personalBest(scores: List[Int]): Int = scores.max

  def report(scores: List[Int]): String = {
    val high = personalBest(scores)
    val last = latest(scores)
    val lastStr = s"Your latest score was ${last}."
    high match {
      case best if last > high => s"${lastStr} That's your personal best!"
      case _ => s"${lastStr} That's ${high - last} short of your personal best"
    }
  }
}
