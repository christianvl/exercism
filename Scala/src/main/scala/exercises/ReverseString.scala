object ReverseString {

  def reverse(str: String): String = {
    str.reverse
    // (str.length -1 until -1 by -1).foldLeft(z = "")((z, x) => s"$z${str(x)}")
    // ((str.length-1 to 0 by -1).map(str(_))).mkString
    // (for(i <- str.length - 1 to 0 by -1) yield str(i)).mkString
    // str.indices.map(i => str.charAt(str.length - i  - 1)).mkString
  }
}
