object Strain {

  def keep[A](coll: Iterable[A], predicate: A => Boolean): Iterable[A] = {
    coll.flatMap(i => {
      i match {
       case c if predicate(c) => Some(c)
       case _ => None }
       }).asInstanceOf[Iterable[A]]
  }

   def discard[A](coll: Iterable[A], predicate: A => Boolean): Iterable[A] = {
    keep(coll, (x: A) => !predicate(x))
  }

}
