object Sieve {

  def primes(max: Int): List[Int] = {
    max match {
      case invalid if (max <= 1)  => List.empty
      case _                      => {
        val allNumbers: List[Int] = (2 to max).toList
        val composites = (2 to math.sqrt(max).toInt).flatMap(i => {
          (0 to max)
            .map(j => math.pow(i, 2).toInt + (j * i))
            .takeWhile(_ <= max)
          })
        allNumbers.filterNot(composites.contains(_))
      }
    }

  }
}
