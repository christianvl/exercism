object Etl {
  def transform(scoreMap: Map[Int, Seq[String]]): Map[String, Int] = {
    scoreMap.flatMap( { case (k,v) => v.map(i => (i.toLowerCase,k)) } ) 
  }
}
