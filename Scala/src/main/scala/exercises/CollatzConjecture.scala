import scala.annotation.tailrec

object CollatzConjecture {

  def steps(n: Int): Option[Int] = {
    n match {
      case invalid if n <= 0 => None
      case _ => Option(collatz(n))
    }
  }

  @tailrec
  def collatz(current: Int, steps: Int = 0): Int = {
    current match {
      case 1 => steps
      case even if current % 2 == 0 => collatz(current / 2, steps + 1)
      case _ => collatz(3 * current + 1, steps + 1)
    }
  }

}
