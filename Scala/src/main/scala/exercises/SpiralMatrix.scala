object SpiralMatrix {

  def spiralMatrix(size: Int): List[List[Int]] = {
    List.range(0, size).map(row =>
      List.range(0, size).map( col =>
        getMatrixNumberAt(size, 1, row, col)))
  }

  def getMatrixNumberAt(currMatrixSize: Int, number: Int, rowIndex: Int, colIndex: Int): Int =
    (rowIndex, colIndex) match {
      case (0, _)                                  => number + colIndex
      case right  if colIndex == currMatrixSize -1 => number + currMatrixSize + rowIndex -1
      case bottom if rowIndex == currMatrixSize -1 => number + currMatrixSize * 3 - 3 - colIndex
      case (_, 0)                                  => number + currMatrixSize * 4 - 4 - rowIndex
      case _                                       =>
        getMatrixNumberAt(currMatrixSize -2, number + currMatrixSize * 4 - 4, rowIndex -1, colIndex -1)
    }

}
