object Pangrams {
  def isPangram(input: String): Boolean = input
  .toLowerCase
  .toSet
  .filter(x => x.toInt > 96 && x.toInt < 123)
  .size >= 26
}
