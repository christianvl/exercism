object BeerSong {
  def verse(i: Int): String = {
    i match {
      case 2 => "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n"
      case 1 => "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n"
      case 0 => "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
      case negative if i <0 => ""
      case _ => s"$i bottles of beer on the wall, $i bottles of beer.\nTake one down and pass it around, ${i -1} bottles of beer on the wall.\n"
    }
  }

  def recite(bottles: Int, i: Int, str: String = ""): String = {
    val x = (str +: verse(bottles)).mkString
    if (i > 1) recite(bottles -1, i -1, s"$x\n") else x
  }
}
