import scala.util.Try
import scala.util.{Failure, Success, Try}
import scala.annotation.tailrec

object BinarySearch extends App {

  @tailrec
  def find(list: List[Int], i: Int, left: Int = 0, right: Int = -1): Option[Int] = {
	  val r: Int = if right == -1 then list.size - 1 else right
	  val mid: Int = left + (r - left) / 2
	  val test: Try[Int] = Try(list(mid))
	  test match {
	    case Failure(exception)    => None
	    case x if test.get == i    => Some(mid)
	    case higher if test.get < i => find(list, i, mid + 1, r)
	    case lower if test.get > i  => find(list, i, left, mid - 1)
	    case _                     => None
	  }
	}

  def find2(nums: List[Int], x: Int): Option[Int] = {
    var left = 0
    var right = nums.length - 1
    while (left <= right) {
      val mid = left + (right - left) / 2
      val number: Int = nums(mid)
      number match {
        case found if number == x => return Some(mid)
        case higher if number < x => left = mid + 1
        case lower if number > x  => right = mid - 1
      }
    }
    return None
  }

}
