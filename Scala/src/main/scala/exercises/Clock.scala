case class Clock(h: Int, m: Int) {

  private val asMinutes = h * 60 + m

  def +(otherClock: Clock): Clock = {
    Clock(asMinutes + otherClock.asMinutes)
  }

  def -(otherClock: Clock): Clock = {
    Clock(asMinutes - otherClock.asMinutes)
  }

}

object Clock {

  def apply(m: Int): Clock = Clock(0,m)

  def apply(h: Int, m: Int): Clock = {
    val asModMinutes = Math.floorMod(60 * h + m, 1440)
    new Clock(Math.floorMod(asModMinutes / 60, 24),Math.floorMod(asModMinutes, 60))
  }

}
