object PhoneNumber {
  def clean(number: String): Option[String] = {
    val filtered = number.filter(c => c.toInt >= 48 && c.toInt <= 57)
    val valid: String = filtered.size match {
      case ten if ten == 10       => filtered
      case eleven if eleven == 11 => if filtered.head.asDigit == 1 then filtered.tail else "0000"
      case _                      => "0000"
    }
    if valid.head.asDigit > 1 && valid(3).asDigit > 1 then Some(valid) else None
  }
}
