object Raindrops {
  def convert(n: Int): String = {
    val result = s"${if n % 3 == 0 then "Pling" else ""}" +
                 s"${if n % 5 == 0 then "Plang" else ""}" +
                 s"${if n % 7 == 0 then "Plong" else ""}"
    if result.isEmpty then n.toString else result
  }
}
