object Twofer {
  def twofer(name: String = ""): String = {
    val finalName = if (name.isEmpty) "you" else name
    s"One for $finalName, one for me."
  }
}
