object NumberType {
 sealed trait Type
 case object Perfect extends Type
 case object Abundant extends Type
 case object Deficient extends Type
}

object PerfectNumbers {

  def classify(n: Int): Either[String, NumberType.Type] = {
    if n <= 0 then
      Left("Classification is only possible for natural numbers.")
      else {
        val aliquot = (1 to n/2).filter(n % _ == 0).sum
        aliquot match {
          case dfc if dfc < n => Right(NumberType.Deficient)
          case abn if abn > n => Right(NumberType.Abundant)
          case _              => Right(NumberType.Perfect)
        }
      }
  }
}
