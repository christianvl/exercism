object SumOfMultiples {

  def sum(factors: Set[Int], limit: Int): Int = {
    factors.flatMap(n => Range(n, limit, n)).sum
  }
}
