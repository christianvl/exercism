import scala.util.{Try, Success}

object RnaTranscription {

  val conversionTable = Map('G' -> 'C', 'C' -> 'G', 'T' -> 'A', 'A' -> 'U')

  def toRna(dna: String): Option[String] = {
    Try(dna.map(conversionTable(_))).toOption
  }

}
