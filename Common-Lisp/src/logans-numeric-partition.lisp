(defpackage :logans-numeric-partition
  (:use :cl)
  (:export :categorize-number :partition-numbers))

(in-package :logans-numeric-partition)

;; Define categorize-number function
(defun categorize-number (alist n)
  (if (oddp n)
      (cons (cons n (car alist)) (cdr alist))
      (cons (car alist) (cons n (cdr alist)))))

(defun categorize-number (acc n)
  (if (oddp n)
      (cons (append (list n) (car acc)) (cdr acc))
      (cons (car acc) (append (list n) (cdr acc)))))

;; Define partition-numbers function
(defun partition-numbers (alist)
  (reduce #'categorize-number alist :initial-value '(NIL)))
