(defpackage :amrstrong-numbers
  (:use :cl)
  (:export :amrstrong-number-p))

(in-package :armstrong-number)

(defun armstrong-number-p (number)
  (let* ((digits (map 'list #'digit-char-p (format nil "~D" number)))
         (power (length digits))
         (candidate (reduce #'+ (map 'list (lambda (x) (expt x power)) digits))))
    (= number candidate)))
