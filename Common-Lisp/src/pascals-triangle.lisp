(defpackage :pascals-triangle
  (:use :cl)
  (:export :rows))
(in-package :pascals-triangle)

(defun build-next-row (previous-row)
  (let ((new-row '(1)))
    (append new-row (loop for i to (- (length previous-row) 2) collect
                                                               (+ (nth i previous-row) (nth (1+ i) previous-row))) '(1))))

(defun rows (n)
  (unless (<= n 0)
    (let ((triangle '((1))))
          (unless (= n 1)
            (loop for row from 2 to n do
              (let ((new-row (list (build-next-row (nth (- row 2) triangle)))))
                (setf triangle (append triangle new-row)))))
      triangle)))
