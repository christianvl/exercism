(defpackage :strain
  (:use :cl)
  (:export :keep :discard))

(in-package :strain)

(defmacro create-filter (name keep?)
  `(defun ,name (predicate list)
     (loop :for i :in list ,(if keep? :when :unless) (funcall predicate i)
           :collect i)))

(create-filter keep t)

(create-filter discard nil)
