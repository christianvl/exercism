(defpackage :prime-factors
  (:use :cl)
  (:export :factors))

(in-package :prime-factors)

(defun factors (n &optional (divisor 2) (factors-list '()))
  (if (> n 1)
    (if (zerop (mod n divisor))
        (factors (/ n divisor) divisor (append factors-list (list divisor)))
        (factors n (1+ divisor) factors-list))
    factors-list))
