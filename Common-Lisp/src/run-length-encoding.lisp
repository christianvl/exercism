(defpackage :run-length-encoding
  (:use :cl)
  (:export :encode
   :decode))

(in-package :run-length-encoding)

(defun concat (str nbr chr)
  (concatenate 'string str (format nil "~a~a" (if (= nbr 1) "" nbr) chr)))

(defun encode (plain)
  (if (< (length plain) 2)
      plain
      (let ((encoded-str "")
            (stack (list 1 (elt plain 0))))
        (loop :for c :across (subseq plain 1)
              :do (if (char= c (second stack))
                      (incf (first stack))
                      (progn
                        (setf encoded-str (concat encoded-str (first stack) (second stack)))
                        (setf stack (append (list 1 c) stack))))
              :finally (setf encoded-str (concat encoded-str (first stack) (second stack))))
        encoded-str)))

(defun decode (compressed)
    (let ((decoded (loop :for i :to (1- (length compressed))
                         :for c = (digit-char-p (elt compressed i))
                         :when c
                           :do (incf i)
                               (when (digit-char-p (elt compressed i))
                                 (setf c (+ (* 10 c) (digit-char-p (elt compressed i))))
                                 (incf i))
                         :collect (make-string (if c c 1) :initial-element (elt compressed i)))))
      (format nil "~{~a~}" decoded)))
