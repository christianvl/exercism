(defpackage :scrabble-score
  (:use :cl)
  (:export :score-word))

(in-package :scrabble-score)

                                 ;; A B C D E F G H I J K L M N O P Q  R S T U V W X Y Z
(defparameter +score-table+ (vector 1 3 3 2 1 4 2 4 1 8 5 1 3 1 1 2 10 1 1 1 1 4 4 8 4 10))

(defun score-word (word)
  "Computes the score for an entire word."
  (loop for c across word when (alpha-char-p c) sum (elt +score-table+ (- (char-code (char-upcase c)) 65))))
