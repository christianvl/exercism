(defpackage :triangle
  (:use :cl)
  (:export :triangle-type-p))

(in-package :triangle)

(defun valid-trianglep (a b c)
  (> (+ a b c) (* 2 (max a b c))))

(defun triangle-type-p (type a b c)
  "Deterimines if a triangle (given by side lengths A, B, C) is of the given TYPE"
  (when (valid-trianglep a b c)
    (case type
      (:equilateral (= a b c))
      (:isosceles   (or (= a b) (= a c) (= b c)))
      (:scalene     (/= a b c)))))
