(defpackage :luhn
  (:use :cl)
  (:export :validp))

(in-package :luhn)

(defun validp (input)
  (let* ((v-input (reverse (remove #\Space input)))
         (size (length v-input))
         (nums (unless (or (<= size 1)
                           (notevery #'digit-char-p v-input))
                 (loop :for i from 0 :to (1- size)
                       :for x = (let ((d (- (char-code (elt v-input i)) 48)))
                                  (if (oddp i) (* 2 d) d))
                       :summing (if (> x 9) (- x 9) x)))))
    (when nums (zerop (mod nums 10)))))
