(defpackage :lillys-lasagna-leftovers
  (:use :cl)
  (:export
   :preparation-time
   :remaining-minutes-in-oven
   :split-leftovers))

(in-package :lillys-lasagna-leftovers)

;; Define function preparation-time
(defun preparation-time (&rest rest)
  (let ((layers (length rest)))
    (* layers 19)))

;; Define function remaining-minutes-in-oven
(defun remaining-minutes-in-oven (&optional (oven-time :normal))
  (case oven-time
    (:normal 337)
    (:shorter 237)
    (:very-short 137)
    (:longer 437)
    (:very-long 537)
    (otherwise 0)))

;; Define function split-leftovers
(defun split-leftovers (&key
                          (weight 0 weight-supplied-p)
                          (alien 10)
                          (human 10))
  (cond
    ((and weight-supplied-p (eql weight nil)) :looks-like-someone-was-hungry)
    ((not weight-supplied-p) :just-split-it)
    (t (- weight alien human))))
