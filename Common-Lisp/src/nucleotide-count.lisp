(defpackage :nucleotide-count
  (:use :cl)
  (:export :dna-count :nucleotide-counts :invalid-nucleotide))

(in-package :nucleotide-count)

(defparameter *table*
  (reduce #'(lambda (lst kv-pair) (setf (gethash (first kv-pair) lst) (second kv-pair)) lst)
          '((#\A 0) (#\T 0) (#\C 0) (#\G 0))
          :initial-value (make-hash-table)))

(define-condition invalid-nucleotide (simple-error) ())

(defun dna-count (nucleotide strand)
  "Returns a count of the given nucleotide appearing in a DNA strand."
  (unless (gethash nucleotide *table*) (error 'invalid-nucleotide))
  (count nucleotide strand))

(defun nucleotide-counts (strand)
  "Returns a hash of nucleotides and their counts in a given DNA strand."
  (map 'list (lambda (x) (setf (gethash x *table*) (dna-count x strand))) strand)
  *table*)
