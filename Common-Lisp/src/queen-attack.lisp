(defpackage :queen-attack
(:use :cl)
(:export :valid-position-p
 :attackp))

(in-package :queen-attack)

(defun valid-position-p (coordinates)
  (and (<= 0 (car coordinates) 7) (<= 0 (cdr coordinates) 7)))

(defun attackp (white-queen black-queen)
  (let ((xw (car white-queen)) (yw (cdr white-queen))
        (xb (car black-queen)) (yb (cdr black-queen)))
    (or (= xw xb) (= yw yb) (= (- yw xw) (- yb xb)) (= (+ yw xw) (+ yb xb)))))
