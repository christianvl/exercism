(defpackage :allergies
  (:use :cl)
  (:shadow :list)
  (:export :allergic-to-p :list))

(in-package :allergies)

(defparameter *allergies* '("eggs" "peanuts" "shellfish" "strawberries" "tomatoes" "chocolate" "pollen"
                            "cats"))

(defun allergic-to-p (score allergen)
  "Returns true if given allergy score includes given allergen."
  (let ((index (position allergen *allergies* :test #'string=)))
    (when index (logbitp index score))))

(defun list (score)
  "Returns a list of allergens for a given allergy score."
  (let ((results ()))
    (dolist (allergie *allergies*) (when (allergic-to-p score allergie) (push allergie results)))
  (reverse results)))
