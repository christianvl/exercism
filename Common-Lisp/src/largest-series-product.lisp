(defpackage :largest-series-product
  (:use :cl)
  (:export :largest-product))

(in-package :largest-series-product)

(defun largest-product (digits span)
  (unless (or (< (length digits) span) (minusp span) (notevery #'digit-char-p digits))
      (loop :for i :to (- (length digits) span)
            :for str = (subseq digits i (+ i span))
            :maximize (reduce #'* (map 'list #'digit-char-p str)))))
