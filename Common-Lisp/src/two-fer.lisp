(defpackage :two-fer
  (:use :cl)
  (:export :two-fer))

(in-package :two-fer)

(defun twofer (&optional name)
  (let* ((n (string-trim " " name))
        (str
          (if (or
               (string= "" n)
               (not name))
              (format nil "you")
              n)))
    (format nil "One for ~a, one for me." str)))

;; other solution, but will not convert an empty string to "you"
;; (defun twofer (&optional name)
;;   (format nil "One for ~a, one for me." (or name "you")))
