(defpackage :rna-transcroption
  (:use :cl)
  (:export :to-rna))

(in-package :rna-transcription)

(defun to-rna (dna)
  "Transcribes a string representing DNA nucleotides to RNA."
  (let* ((table (pairlis '(#\G #\C #\T #\A) '(#\C #\G #\A #\U)))
         (translation (map 'list (lambda (x) (cdr (assoc x table))) dna))
         (validated (notany #'null translation)))
    (if validated
        (concatenate 'string translation)
        (error "Invalid DNA"))))
