(defpackage :isbn-verifier
  (:use :cl)
  (:export :validp))

(in-package :isbn-verifier)

(defun validp (isbn)
  ;; ensure the string is of length 10 without symbols
  (when (= (length (remove-if-not #'alphanumericp isbn)) 10)
    ;; covert isbn string to list of ints
    (let ((check (remove-if #'not
                            (loop :for i :to (1- (length isbn))
                                  :collect (if (= i (1- (length isbn)))
                                               ;; check if last char in isbn string is x and assign
                                               (last-x-p (elt isbn i))
                                               (digit-char-p (elt isbn i)))))))
      ;; check if list of ints is of valid length
      (when (= 10 (length check))
        ;; multiply each element, reduce, mod 11 and return zerop result
        (zerop (mod (loop :for i :to 9
                          :for n :from 10 :downto 1
                          :summing (* (elt check i) n)) 11))))))

(defun last-x-p (x)
  (if (char= (char-upcase x) #\X) 10 (digit-char-p x)))
