(defpackage :phone-number
  (:use :cl)
  (:export :clean))

(in-package :phone-number)

(defun valid? (num-string)
  (and (<= 50 (char-code (elt num-string 0)) 57)
       (<= 50 (char-code (elt num-string 3)) 57)))

(defun clean (phrase)
  "Converts a PHRASE string into a string of digits.
Will evaluate to \"0000000000\" in case of an invalid input."
  (let ((number (remove-if-not #'digit-char-p phrase)))
    (when (and (= 11 (length number)) (char= (elt number 0) #\1)) (setf number (subseq number 1)))
    (if (and (= 10 (length number)) (valid? number))
        number
        "0000000000")))
