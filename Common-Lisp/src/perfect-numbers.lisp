(defpackage :perfect-numbers
  (:use :cl)
  (:export :classify))

(in-package :perfect-numbers)

(defun factors-of (n)
 (loop for divisor from 1 to (/ n 2)
       when (zerop (rem n divisor)) collect divisor))

(defun classify (n)
  (when (plusp n)
    (let ((aliquot-sum (reduce #'+ (factors-of n))))
      (cond
      ((= aliquot-sum n) "perfect")
      ((< aliquot-sum n) "deficient")
      ((> aliquot-sum n) "abundant")))))
