(defpackage :sieve
  (:use :cl)
  (:export :primes-to)
  (:documentation "Generates a list of primes up to a given limit."))

(in-package :sieve)

(defun primes-to (n)
  "List primes up to `n' using sieve of Eratosthenes."
  (let ((not-primes ()))
    (loop :for i :from 2 :to n
          :unless (member i not-primes) :collect i
            :do (loop :for j :from 2 :to (sqrt n) :do
              (let ((num (* i j)))
                (push num not-primes)
                (when (> num n) (return)))))))
