(defpackage :isogram
  (:use :cl)
  (:export isogram-p))

(in-package :isogram)

(defun isogram-p (string)
"Test if a string is an isogram"
  (let ((test-string (string-downcase (remove-if-not #'alpha-char-p string))))
    (every (lambda (x) (= 1 (count x test-string))) test-string)))

(defun isogram2-p (string)
  "Test if a string is an isogram"
  (let ((test-string (remove-if-not #'alpha-char-p string)))
    (string-equal (remove-duplicates test-string :test #'char-equal) test-string)))

