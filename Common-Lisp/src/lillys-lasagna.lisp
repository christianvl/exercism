(defpackage :lillys-lasagna
  (:use :cl)
  (:export :expected-time-in-oven
           :remaining-minutes-in-oven
           :preparation-time-in-minutes
           :elapsed-time-in-minutes))

(in-package :lillys-lasagna)

;; Define function expected-time-in-oven
(defun expected-time-in-oven () 
  "function that does not take any parameters and returns how many minutes the lasagna should be in the oven. According to the Lisp Alien tradition (just like Lilly's parental-unit used to cook), the expected oven time in minutes is 337."
  337)

;; Define function remaining-minutes-in-oven
(defun remaining-minutes-in-oven (actual-minutes)
  "function that takes the actual minutes the lasagna has been in the oven as a parameter and returns how many minutes the lasagna still has to remain in the oven, based on the expected oven time in minutes from the previous task."
  (- (expected-time-in-oven) actual-minutes ))

;; Define function preparation-time-in-minutes
(defun preparation-time-in-minutes (layers)
  "function that takes the number of layers Lilly added to the lasagna as a parameter and returns how many minutes Lilly spent preparing the lasagna, assuming each layer takes 19 minutes to prepare."
  (* 19 layers))

;; Define function elapsed-time-in-minutes
(defun elapsed-time-in-minutes (layers oven-time)
  "function that takes two parameters: the first parameter is the number of layers Lilly added to the lasagna, and the second parameter is the number of minutes the lasagna has been in the oven. The function should return how many minutes Lilly has worked on cooking the lasagna, which is the sum of the preparation time in minutes, and the time in minutes the lasagna has spent in the oven at the moment."
  (+ oven-time (preparation-time-in-minutes layers)))
