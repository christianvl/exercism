(defpackage :leap
  (:use :cl)
  (:export :leap-year-p))

(in-package :leap)

(defun leap-year-p (year)
  (if (= 0 (mod year 100))
      (= 0 (mod year 400))
      (= 0 (mod year 4))))

(defun leap-year-p (year)
  (if (zerop (mod year 100))
      (zerop (mod year 400))
      (zerop (mod year 4))))




