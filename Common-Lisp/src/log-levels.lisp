(defpackage :log-levels
  (:use :cl)
  (:export :log-message :log-severity :log-format))

(in-package :log-levels)

(defun log-message (long-string)
  (string-trim '(#\Space) (subseq long-string 7)))

(defun log-severity (long-string)
  (let ((severity (subseq long-string 1 5)))
    (cond
      ((string-equal severity "info") :everything-ok)
      ((string-equal severity "warn") :getting-worried)
      ((string-equal severity "ohno") :run-for-cover))))

(defun log-format (long-string)
  (let ((severity (log-severity long-string))
        (message (log-message long-string)))
    (case severity
      (:everything-ok (string-downcase message))
      (:getting-worried (string-capitalize message))
      (:run-for-cover (string-upcase message)))))
