(defpackage :matching-brackets
  (:use :cl)
  (:export :pairedp))

(in-package :matching-brackets)

(defun pairedp (value)
  (let ((bracks (list #\{ #\} #\[ #\] #\( #\)))
        (track-open (list)))
    (loop :for i :across value
          :for b = (position i bracks)
          :when b
            :do (cond
                  ((evenp b) (push b track-open))
                  ((oddp b) (if (equal (1- b) (first track-open))
                                (pop track-open)
                                (return (setf track-open -1))))))
    (null track-open)))
