(defpackage :twelve-days
  (:use :cl)
  (:export :recite))

(in-package :twelve-days)

(defvar *things* '("a Partridge in a Pear Tree" "two Turtle Doves" "three French Hens"
                   "four Calling Birds" "five Gold Rings" "six Geese-a-Laying"
                   "seven Swans-a-Swimming" "eight Maids-a-Milking" "nine Ladies Dancing"
                   "ten Lords-a-Leaping" "eleven Pipers Piping" "twelve Drummers Drumming"))

(defun verse-builder (start &optional (verse start) (verses-str ""))
  "helper function, returns a string of one verse for the 12 Days of Christmas"
  (unless (> 13 start 0) (error "Illegal verse index"))
  (if (= 1 verse)
      (format NIL "On the ~:r day of Christmas my true love gave to me: ~A~A~A."
              start
              verses-str
              (if (= 1 verse start) "" "and ")
              (elt *things* (1- verse)))
      (verse-builder start (1- verse)
                     (concatenate 'string verses-str
                                  (format NIL "~A, " (elt *things* (1- verse)))))))

(defun recite (&optional begin (end begin) (song-str ""))
  "Returns a string of the requested verses for the 12 Days of Christmas."
  (when (null begin) (setf begin 1) (setf end 12))
  (let ((song (format nil "~A~&~A" song-str (verse-builder begin))))
    (if (< begin end)
        (recite (1+ begin) end song)
        (format NIL "~A" song))))
