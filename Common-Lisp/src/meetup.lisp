(defpackage :meetup
  (:use :cl)
  (:export :meetup))

(in-package :meetup)

(defun get-weekday (year month &optional (day 1))
  "https://en.wikipedia.org/wiki/Zeller%27s_congruence"
  (let* ((adjust (<= month 2))
         (a-month (if adjust (+ month 12) month))
         (a-year (if adjust (- year 1) year))
         (K (mod a-year 100))
         (J (floor a-year 100))
         (wd (mod (+ day
                     (floor (* 13 (+ a-month 1)) 5)
                     K
                     (floor K 4)
                     (floor J 4)
                     (- (* 2 J)))
                  7)))
        (nth wd '(:saturday :sunday :monday :tuesday :wednesday :thursday :friday))))

(defun leap-year-p (year)
  (if (= 0 (mod year 100))
      (= 0 (mod year 400))
      (= 0 (mod year 4))))

(defun meetup (month year dow schedule)
  "Returns a date in the format (y m d) for a given meetup date."
  (let* ((one (if (equal schedule :teenth) 13 1))
         (seven (+ one 6))
         (day (loop :for i :from one :to seven
                    :when (equal dow (get-weekday year month i))
                      :return (case schedule
                (:second (+ i 7))
                (:third  (+ i 14))
                (:fourth (+ i 21))
                (:last (cond
                         ((= month 2) (if (leap-year-p year) (+ i 28) (+ i 21)))
                         ((find month '(1 3 5 7 8 10 12)) (if (<= i 3) (+ i 28) (+ i 21)))
                         (t (if (<= i 2) (+ i 28) (+ i 21)))))
                (otherwise i)))))
          (list year month day)))
