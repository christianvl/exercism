(defpackage :protein-translation
  (:use :cl)
  (:export :proteins
           :invalid-protein))

(in-package :protein-translation)

(defvar *proteins-table*
  '(("AUG" . "Methionine")
    ("UUU" . "Phenylalanine") ("UUC" . "Phenylalanine")
    ("UUA" . "Leucine") ("UUG" . "Leucine")
    ("UCU" . "Serine") ("UCC" . "Serine") ("UCA" . "Serine") ("UCG" . "Serine")
    ("UAU" . "Tyrosine") ("UAC" . "Tyrosine")
    ("UGU" . "Cysteine") ("UGC" . "Cysteine")
    ("UGG" . "Tryptophan")
    ("UAA" . "STOP") ("UAG" . "STOP") ("UGA" . "STOP")))

(define-condition invalid-protein (error)
  ((text :initarg :text :reader text)))

(defun codon-protein (codon)
  (let ((protein (cdr (assoc codon *proteins-table* :test #'string=))))
    (if protein protein (error 'invalid-protein :text codon))))

(defun proteins (strand)
  (loop :for start :below (length strand) :by 3
        :for end := (if (<= (+ start 3) (length strand)) (+ start 3) (error 'invalid-protein :text strand))
        :for protein := (codon-protein (subseq strand start end))
        :while (string/= protein "STOP")
        :collect protein))
