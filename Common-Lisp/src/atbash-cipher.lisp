(defpackage :atbash-cipher
  (:use :cl)
  (:export :encode))

(in-package :atbash-cipher)

(defparameter *cipher* (pairlis '(#\z #\y #\x #\w #\v #\u #\t #\s #\r #\q #\p #\o #\n #\m #\l #\k #\j #\i #\h
                                  #\g #\f #\e #\d #\c #\b #\a)
                                '(#\a #\b #\c #\d #\e #\f #\g #\h #\i #\j #\k #\l #\m #\n #\o #\p #\q #\r #\s
                                  #\t #\u #\v #\w #\x #\y #\z)))

;; only iterates over the original string once
(defun encode (plaintext)
  (let* ((pre-cipher-text (remove-if-not #'alphanumericp plaintext))
         (cipher ""))
    (loop :for x :to (1- (length pre-cipher-text)) :do
      (let ((chr (char-downcase (elt pre-cipher-text x))))
        (setf chr (if (alpha-char-p chr) (cdr (assoc chr *cipher*)) chr))
        (setf cipher (concatenate 'string cipher (if (and (zerop (mod x 5)) (plusp x))
                                                     (format nil " ~A" chr)
                                                     (format nil "~A" chr))))))
    cipher))


(defun encode1 (plaintext)
  (let* ((pre-cipher-text
           (map 'string (lambda (x) (if (alpha-char-p x)
                                        (cdr (assoc (char-downcase x) *cipher*))
                                        x)) (remove-if-not #'alphanumericp plaintext))))
    (apply #'concatenate 'string (loop :for i :from 0 :to (1- (length pre-cipher-text))
                                       :collect (if (and (plusp i) (zerop (mod i 5)))
                                                    (format nil " ~A" (elt pre-cipher-text i))
                                                    (format nil "~A" (elt pre-cipher-text i)))))))
