(defpackage :etl
  (:use :cl)
  (:export :transform))

(in-package :etl)

(defun transform (h-table)
  "Transforms hash values into keys with their keys as their values."
  (let ((new-table (make-hash-table)))
    (maphash (lambda (number list-chars)
               (dolist (a-char list-chars)
                 (setf (gethash (char-downcase a-char) new-table) number)))
             h-table)
    new-table))
