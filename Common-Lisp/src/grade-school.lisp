(defpackage :grade-school
  (:use :cl)
  (:export :make-school :add :roster :grade))

(in-package :grade-school)

(defun make-school ()
  (make-hash-table))

(defun roster (school)
  (let ((names '())
        (grades (sort (loop for k being the hash-keys in school collect k) #'<)))
    (setf names
          (loop for k in grades collect (sort (gethash k school) #'string<)))
    (apply #'append names)))

(defun add (school name grade)
  (unless (member name (roster school) :test #'string-equal)
    (push name (gethash grade school))))

(defun grade (school grade)
  (sort (gethash grade school) #'string<))
