(defpackage :robot-simulator
  (:use :cl)
  (:export :+north+ :+east+ :+south+ :+west+ :execute-sequence
   :robot :robot-position :robot-bearing :make-robot))

(in-package :robot-simulator)

(defconstant +north+ 0)
(defconstant +east+  1)
(defconstant +south+ 2)
(defconstant +west+  3)

(defclass robot () ((pos :initarg :pos :accessor robot-position)
                    (bea :initarg :bea :accessor robot-bearing)))

(defun make-robot (&key (position '(0 . 0)) (bearing +north+))
  (make-instance 'robot :pos position :bea bearing))

(defun advance (robot)
  (let ((x (car (robot-position robot)))
        (y (cdr (robot-position robot)))
        (bea (robot-bearing robot)))
    (cond
      ((= bea +north+) (incf y))
      ((= bea +east+)  (incf x))
      ((= bea +south+) (decf y))
      ((= bea +west+)  (decf x)))
    (cons x y)))

(defun execute-sequence (robot seq-str)
    (loop :for mov :across seq-str :do
      (let ((bearing (robot-bearing robot)))
        (cond
          ((char= mov #\R) (setf (robot-bearing robot) (if (= bearing +west+) +north+ (1+ bearing))))
          ((char= mov #\L) (setf (robot-bearing robot) (if (= bearing +north+) +west+ (1- bearing))))
          ((char= mov #\A) (setf (robot-position robot) (advance robot)))))) robot)
