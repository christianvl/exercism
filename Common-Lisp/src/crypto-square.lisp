(defpackage :crypto-square
  (:use :cl)
  (:export :encipher))
(in-package :crypto-square)

(defun normalize (plaintext)
  (remove-if-not #'alphanumericp (string-downcase plaintext)))

(defun get-rectangle-dim (norm-text)
  (let* ((l (length norm-text))
         (dim (sqrt l)))
    (cons (ceiling dim) (round dim))))

(defun text-to-rectangle (text col-dim)
  (let ((l (length text)))
  (loop :for i :to (1- l) :by col-dim
        :collect (let ((token (if (<= l (* col-dim col-dim))
                                  (subseq text i (unless (< l (+ i col-dim)) (+ i col-dim)))
                                  (progn (let ((pad (rem l col-dim)))
                                           (cond
                                             ((<= i (* pad col-dim)) (subseq text i (+ i col-dim)))
                                             ((= i (+ col-dim (* pad col-dim))) (subseq text i (1- (+ i col-dim))))
                                             (t (subseq text (1- i) (+ (- i (- col-dim pad)) col-dim))))))))
                                  (directive (format nil "~~~aa" col-dim)))
                       (format nil directive token)))))

(defun encode-rectangle-text (rectangle-text)
  (let ((encoded-text ""))
    (loop :for i :to (1- (length (first rectangle-text))) :do
      (setf encoded-text (concatenate 'string encoded-text (map 'string (lambda (word) (elt word i)) rectangle-text))))
    (normalize encoded-text)))

(defun encoded-rectangle-to-string (encoded-rectangle)
  (let ((str ""))
    (loop :for word :to (1- (length encoded-rectangle)) :do
          (setf str (concatenate 'string str (when (> word 0) " ") (nth word encoded-rectangle)))) str))

(defun encipher (plaintext)
  (let* ((wip (normalize plaintext))
         (size (length wip))
         (dim (get-rectangle-dim wip)))
    (unless (<= size 2)
      (setf wip (text-to-rectangle wip (car dim)))
      (setf wip (encode-rectangle-text wip))
      (setf wip (text-to-rectangle wip (cdr dim)))
      (setf wip (encoded-rectangle-to-string wip)))
    wip))
