(defpackage :all-your-base
  (:use :cl)
  (:export :rebase))

(in-package :all-your-base)

(defun convert-to-decimal (digits from-base &optional converted)
  (if (null digits)
      (map 'list #'digit-char-p (write-to-string (reduce #'+ converted)))
      (let* ((power (1- (length digits)))
             (curr-digit (unless (zerop (car digits)) (list (* (car digits) (expt from-base power))))))
        (convert-to-decimal (cdr digits) from-base (append converted curr-digit)))))

(defun convert-from-decimal (decimal-num to-base &optional list)
  (if (zerop decimal-num)
      list
      (multiple-value-bind (d r) (floor decimal-num to-base)
        (convert-from-decimal d to-base (append (list r) list)))))

(defun rebase (list-digits in-base out-base)
  (cond
    ((or (<= in-base 1) (<= out-base 1)) ())
    ((null list-digits) '(0))
    ((every #'zerop list-digits) '(0))
    ((some (lambda (x) (>= x in-base)) list-digits) ())
    ((some (lambda (x) (< x 0)) list-digits) ())
    ((= 10 in-base) (convert-from-decimal (nth-value 0 (parse-integer (format nil "~{~A~}" list-digits))) out-base))
    ((= 10 out-base) (convert-to-decimal list-digits in-base))
    (t (rebase (rebase list-digits in-base 10) 10 out-base))))
