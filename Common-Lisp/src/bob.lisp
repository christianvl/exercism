(defpackage :bob
  (:use :cl)
  (:export :response))
(in-package :bob)

(defun response (hey-bob)
  (let* ((is-empty (string= "" (remove #\Space (remove #\Tab (remove #\Newline hey-bob)))))
         (tester (remove-if-not (lambda (ch) (>= 90 (char-code (char-upcase ch)) 65)) hey-bob))
         (is-yelling (if (string= "" tester) NIL (string= tester (string-upcase tester))))
         (is-question (if is-empty NIL (char= #\? (elt (string-left-trim '(#\Space) (reverse hey-bob)) 0)))))
    (cond
      (is-empty                     "Fine. Be that way!")
      ((and is-yelling is-question) "Calm down, I know what I'm doing!")
      (is-question                  "Sure.")
      (is-yelling                   "Whoa, chill out!")
      (t                            "Whatever."))))
