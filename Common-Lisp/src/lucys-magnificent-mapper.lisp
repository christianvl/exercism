(defpackage :lucys-magnificent-mapper
  (:use :cl)
  (:export :make-magnificent-maybe :only-the-best))

(in-package :lucys-magnificent-mapper)

;; Define make-magnificent-maybe function
(defun make-magnificent-maybe (func list)
  (mapcar func list))

;; Define only-the-best function
(defun only-the-best (func list)
  ;;(remove-if func (remove 1 list)))
  (remove-if
   (lambda (elm)
     (or
      (= elm 1)
      (funcall func elm)))
   list))
