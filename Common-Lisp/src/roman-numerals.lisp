(defpackage :roman-numerals
  (:use :cl)
  (:export :romanize))

(in-package :roman-numerals)

(defparameter *base-ints* '(1000 900 500 400 100 90 50 40 10 9 5 4 1))
(defparameter *ints-roms* (pairlis *base-ints* '("M" "CM" "D" "CD" "C" "XC" "L" "XL" "X" "IX" "V" "IV" "I")))

(defun get-one-roman (digit)
  (cdr (assoc digit *ints-roms*)))

(defun romanize (number)
  "Returns the Roman numeral representation for a given number."
  (let ((base-roman (find-if (lambda (x) (>= number x)) *base-ints*)))
    (if (null base-roman) ""
        (concatenate 'string
                     (get-one-roman base-roman)
                     (romanize (- number base-roman))))))
