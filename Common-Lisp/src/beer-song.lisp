(defpackage :beer-song
(:use :cl)
(:export :verse :sing))

(in-package :beer-song)

(defun verse (n)
  "Returns a string verse for a given number."
  (case n
    (0
     (format nil "~A~%~A~&"
             "No more bottles of beer on the wall, no more bottles of beer."
             "Go to the store and buy some more, 99 bottles of beer on the wall."))
    (1
     (format nil "~A~%~A~&"
             "1 bottle of beer on the wall, 1 bottle of beer."
             "Take it down and pass it around, no more bottles of beer on the wall."))
    (2
     (format nil "~A~%~A~&"
             "2 bottles of beer on the wall, 2 bottles of beer."
             "Take one down and pass it around, 1 bottle of beer on the wall."))
    (otherwise
     (format nil "~D ~A ~D ~A~%~A ~D ~A~&"
             n "bottles of beer on the wall," n "bottles of beer."
             "Take one down and pass it around," (1- n) "bottles of beer on the wall."))))

(defun sing (start &optional (end 0))
  "Returns a string of verses for a given range of numbers."
  (cond
    ((< start end) "")
    ((>= start 0) (concatenate 'string (verse start) (format nil "~%") (sing (1- start) end)))))
