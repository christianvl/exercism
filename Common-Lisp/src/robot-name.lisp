(defpackage :robot-name
  (:use :cl)
  (:export :build-robot :robot-name :reset-name))

(in-package :robot-name)

(defvar *robots* (make-hash-table))

(defun create-serial-name ()
  (loop :do
    (let ((serial-name '()))
      (dotimes (i 5)
        (setf serial-name (append serial-name
                                  (if (<= i 1)
                                      (list (code-char (- 90 (random 26))))
                                      (list (random 10))))))
      (setf serial-name (format nil "~{~A~}" serial-name))
      (unless (gethash serial-name *robots*) (return serial-name)))))

(defclass robot () ((name
                     :initarg :name :accessor name
                     :initform (create-serial-name))))

(defun build-robot ()
  (let ((robot (make-instance 'robot)))
    (setf (gethash (name robot) *robots*) robot)))

(defun robot-name (robot)
  (name robot))

(defun reset-name (robot)
  (setf (name robot) (create-serial-name)))

