(defpackage :sum-of-multiples
  (:use :cl)
  (:export :sum))

(in-package :sum-of-multiples)

(defun sum (factors limit)
  (let ((points '(0)))
    (dolist (item-value factors)
      (unless (zerop item-value)
        (loop :for i :from 0 :to (1- limit) :by item-value
              :unless (member i points)
                :do (push i points))))
    (reduce #'+ points)))
