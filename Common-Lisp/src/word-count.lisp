(defpackage :word-count
  (:use :cl)
  (:export :count-words))
(in-package :word-count)

(defun clear-symbols (text-str)
  (remove-if-not (lambda (ch) (or
                              (char= #\Space ch)
                              (char= #\' ch)
                              (alphanumericp ch)))
                 (substitute #\Space #\, (substitute #\Space #\Newline text-str))))

(defun split-words (str)
  (let ((start 0)
        (next (position-if (lambda (x) (char= x #\Space)) str))
        (words ()))
    (loop
      (push (string-downcase (subseq str 0 next)) words)
      (unless next (return words))
      (setf start (1+ next))
      (setf str (subseq str start))
      (setf next (position-if (lambda (x) (char= x #\Space)) str)))
    (remove "" words :test #'string=)))

(defun clear-s-quotes (word-str)
  (string-trim '(#\') word-str))

(defun preprocess (text-str)
  (let ((words-bag (split-words (clear-symbols text-str))))
    (map 'list #'clear-s-quotes words-bag)))

(defun count-words (sentence)
  (let* ((original-words (preprocess sentence))
         (unique-words (remove-duplicates original-words :test #'string-equal))
         (counts (map 'list (lambda (x) (count-if (lambda (y) (string-equal x y)) original-words)) unique-words)))
    (pairlis unique-words counts)))
