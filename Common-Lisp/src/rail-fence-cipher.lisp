 (defpackage :rail-fence-cipher
  (:use :cl)
  (:export :encode
   :decode))

(in-package :rail-fence-cipher)

(defun zig-zag (msg rails)
  "Creates a hash-table of the string msg in the number rails"
  (let ((msg-in-rails (make-hash-table))
        (from-top t))
    (loop :for i :to (1- (length msg)) :by (1- rails) ;; loop the length of chars in msg
          ;; create substrings by number of rails -1 (ensure the last substring is not out of bounds)
          :for lst := (+ i (1- rails)) ;; end boundary of substring in this loop iteration
          :for tmp := (subseq msg i (if (<= lst (length msg)) lst (length msg))) ;;substring in this loop iteration
          :do (loop :for rail :to (- rails 2) ;; place each char of substring in the corresponding rail in the hash-table
                                              ;; read to rails -2 because so the last rail becomes the first when reading
                                              ;; in the opposite direction
                    :for idx := (if from-top rail ;; this is the rail key number for the hash-table
                                    (abs (- rail (1- rails))))
                    :when (< rail (length tmp)) ;; only write when the substring fits the rails
                                                ;; avoid errors if the last substring is shorter than rails
                      :do (setf (gethash idx msg-in-rails) ;; append char to rail key in hash-table
                                (append (gethash idx msg-in-rails) (list (elt tmp rail))))
                          ;; when done reading substring, revert reading direction
                    :finally (setf from-top (not from-top)))) msg-in-rails))

(defun encode (msg rails)
  (let ((encoded (zig-zag msg rails)))
    (format nil "~{~A~}" (loop :for v :being :the :hash-values :of encoded
          :collect (format nil "~{~A~}" v)))))

(defun decode (msg rails)
  (let* ((zig-zag-msg (zig-zag msg rails)) ;; put the encoded msg in rails
         (rail 0) ;; helper variable for loops
         (decoded nil) ;; used to store the return value
         (from-top t) ;; helper variable for loops
         (rail-lengths ;; to decode the msg, we need to know how many chars goes in each rail
           (loop :for v :being :the :hash-values :of zig-zag-msg
                 :collect (length v)))
         (decoding ;; ordered list of strings with the chars for each rail
           (loop :for i :below (length msg) ;; loop msg length
                 :for n := (+ i (nth rail rail-lengths)) ;; index to substring encoded-msg by rail
                 :collect (subseq msg i (if (<= n (length msg)) n (length msg))) ;; ensure last substring is within bounds
                 :do (setf i (1- n)) ;; given subseq start index is inclusive, decrement i by one for next iteration
                     (incf rail)))) ;; and move to build substring of the next rail
    (setf rail 0) ;; reset helper variable
    ;; we'll loop always reading the first char of each substring collected in decoding
    ;; remove collected item from decoding list
    ;; Revert the order of reading after reaching top or bottom rail
    (setf decoded (loop for i :below (length msg) ;; number of required reads
                        :when (> (length (nth rail decoding)) 0) ;; because not all substrings have the same length
                          :collect (elt (nth rail decoding) 0)   ;; only collect with substring already not empty
                        :do (unless (zerop (length (nth rail decoding)))
                              (setf (nth rail decoding) (subseq (nth rail decoding) 1))
                              (cond ;; increment/decrement rail index and change reading direction accordingly
                                ((zerop rail) (incf rail) (setf from-top t))
                                ((= rail (1- rails)) (decf rail) (setf from-top nil))
                                (t (if from-top (incf rail) (decf rail)))))))
    (format nil "~{~A~}" decoded)))
