(defpackage :hamming
  (:use :cl)
  (:export :distance))

(in-package :hamming)

(defun distance (dna1 dna2)
  "Number of positional differences in two equal length dna strands."
  (when (= (length dna1) (length dna2))
    (let ((x 0))
      (dotimes (i (length dna1))
        (when (char/= (elt dna1 i) (elt dna2 i)) (setf x (1+ x)))) x)))

(defun distance (dna1 dna2)
  "Number of positional differences in two equal length dna strands."
  (when (= (length dna1) (length dna2))
    (count nil (map 'list #'char= dna1 dna2))))

(defvar d1 "GAGCCTACTAACGGGAT")
(defvar d2 "CATCGTAATGACGGCCT")

(defvar ds (list (map 'list #'identity d1) (map 'list #'identity d2)))
