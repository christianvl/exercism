(defpackage :nth-prime
  (:use :cl)
  (:export :find-prime))

(in-package :nth-prime)

(defun find-prime (number)
  (case number
    (0 nil)
    (1 2)
    (otherwise
     (flet ((primep (number)
              (when (> number 1)
                (loop :for divisor :from 2 :to (sqrt number)
                      :never (zerop (mod number divisor))))))
       (loop :for i :from 3 :by 2
             :when (primep i)
               :sum 1 into nth
             :when (= (1+ nth) number)
               :do (return i))))))
