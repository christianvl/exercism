(defpackage :reporting-for-duty
  (:use :cl)
  (:export :format-quarter-value :format-two-quarters
           :format-two-quarters-for-reading))

(in-package :reporting-for-duty)

;; Define format-quarter-value function.
(defun format-quarter-value (text value)
  (format NIL "The value ~a quarter: ~a" text value))

;; Define format-two-quarters function.
(defun format-two-quarters (s txt1 val1 txt2 val2)
  (format s "~%~a~%~a~%"
          (format-quarter-value txt1 val1)
          (format-quarter-value txt2 val2)))

;; Define format-two-quarters-for-reading function.
(defun format-two-quarters-for-reading (s txt1 val1 txt2 val2)
  (format s "(~s ~s)"
          (format-quarter-value txt1 val1)
          (format-quarter-value txt2 val2)))
