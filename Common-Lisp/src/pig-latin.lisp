(defpackage :pig-latin
  (:use :cl)
  (:export :translate))

(in-package :pig-latin)

(defun first-vowel-position (word)
  (let ((pos (loop :for vowel :in (list #\A #\E #\I #\O #\U)
                   :for i := (position vowel word :test #'char-equal)
                   :when i
                     :collect i)))
    (when pos (apply #'min pos))))

(defun test-rules (word)
  (let ((fst-vowel (first-vowel-position word))
        (xr-present (search "xr" word))
        (yt-present (search "yt" word))
        (qu-present (search "qu" word))
        (y-present (search "y" word)))
    (cond
      ((some (lambda (x) (equal 0 x)) (list fst-vowel xr-present yt-present)) 0)
      (qu-present (if (and fst-vowel (< fst-vowel qu-present))
                      fst-vowel
                      (+ 2 qu-present)))
      (y-present (if (and fst-vowel (< fst-vowel y-present))
                     fst-vowel
                     (if (zerop y-present) (1+ y-present) y-present)))
      (t fst-vowel))))

(defun split-words (phrase)
  (let ((last-word phrase)
        (words-list ()))
    (loop :for i :below (length phrase)
          :for space := (position #\Space phrase :start i :test #'char=)
          :when space
            :do (push (subseq phrase i space) words-list)
                (setf i (+ i space))
                (setf last-word (subseq phrase (1+ space)))
          :finally (push last-word words-list))
    (reverse words-list)))

(defun translate-word (word)
  (let ((index (test-rules word)))
    (concatenate 'string (subseq word index) (subseq word 0 index) "ay")))

(defun translate (phrase)
  (let* ((words (split-words phrase))
         (translations (map 'list #'translate-word words)))
    (format nil "~{~A~^ ~}" translations)))
