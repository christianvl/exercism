(defpackage :pizza-pi
  (:use :cl)
  (:export :dought-calculator :pizza-per-cube
           :size-from-sauce :fair-share-p))

(in-package :pizza-pi)

;; formula to use
;; grams = pizzas * (((45 * pi * diameter) / 20) + 200)
(defun dough-calculator (pizzas diameter)
  (round (* pizzas (+ 200 (/ (* 45 pi diameter) 20)))))

;; formula to use
;; diameter = sqrt ((40 * sauce) / (3 * pi))
(defun size-from-sauce (sauce)
  (sqrt (/ (* 40 sauce) (* 3 pi))))

;; formula to use
;; pizzas = (2 * (side ^ 3)) / (3 * pi * (diameter ^ 2))
(defun pizzas-per-cube (cube-size diameter)
  (floor (/ (* 2 (expt cube-size 3)) (* 3 pi (expt diameter 2)))))

;; pizza must have 8 slices
;; slices must be evenly distributed by friends
(defun fair-share-p (pizzas friends)
  (= 0 (mod (* pizzas 8) friends)))
