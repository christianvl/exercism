(defpackage :collatz-conjecture
  (:use :cl)
  (:export :collatz))

(in-package :collatz-conjecture)

(defun collatz (n &optional (counter 0))
  (cond
    ((not (plusp n)) nil)
    ((= n 1) counter)
    (t (collatz (if (evenp n) (/ n 2) (1+ (* 3 n))) (1+ counter)))))
