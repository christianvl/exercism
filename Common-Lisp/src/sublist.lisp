(defpackage :sublist
  (:use :cl)
  (:export :sublist))

(in-package :sublist)

(defun sublistp (l1 l2)
  "test if list1 is a sublist of list2"
  (when (> (length l2) (length l1))
    (if (null l1) t
        (let ((fst (position (first l1) l2)))
          (when fst (let ((test-list (subseq l2 fst (+ fst (length l1)))))
                      (if
                       (equal test-list l1) t
                       (sublistp l1 (rest l2)))))))))

(defun sublist (list1 list2)
  "what is list1 of list2 (sublist, superlist, equal or unequal)"
  (cond
    ((equal list1 list2) :equal)
    ((sublistp list1 list2) :sublist)
    ((sublistp list2 list1) :superlist)
    (t :unequal)))

;; Using common-lisp built-in tools
;; (defun sublist (list1 list2)
;;   "what is list1 of list2 (sublist, superlist, equal or unequal)"
;;   (cond
;;     ((equal list1 list2) :equal)
;;     ((search list1 list2) :sublist)
;;     ((search list2 list1) :superlist)
;;     (t :unequal)))
