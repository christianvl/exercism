(defpackage :binary-search
  (:use :cl)
  (:export :binary-find :value-error))

(in-package :binary-search)

(defun binary-find (arr el &optional (min 0) (max (1- (length arr))))
  (let* ((search-index (floor (/ (+ min max) 2)))
         (candidate (unless (minusp search-index) (elt arr search-index))))
    (cond
      ((null candidate) nil)
      ((= el candidate) search-index)
      ((or (zerop search-index) (>= search-index max)) nil)
      ((> el candidate) (binary-find arr el (1+ search-index) max))
      ((< el candidate) (binary-find arr el min (1- search-index))))))
