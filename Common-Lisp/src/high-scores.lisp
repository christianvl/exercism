(defpackage :high-scores
  (:use :cl)
  (:export :make-high-scores-table :add-player
           :set-score :get-score :remove-player))

(in-package :high-scores)

;; Define make-high-scores-table function
(defun make-high-scores-table ()
  (make-hash-table))

;; Define add-player function
(defun add-player (table player-name)
  (setf (gethash player-name table) 0))

;; Define set-score function
(defun set-score (table player-name score)
  (setf (gethash player-name table) score))

;; Define get-score function
(defun get-score (table player-name)
  (let ((score (gethash player-name table)))
    (if (not score) 0 score)))

;; Define remove-player function
(defun remove-player (table player-name)
  (remhash player-name table))
