(defpackage :space-age
  (:use :cl)
  (:export    :on-mercury
   :on-venus  :on-earth
   :on-mars   :on-jupiter
   :on-saturn :on-uranus
   :on-neptune))

(in-package :space-age)

(defvar earth-seconds 31557600)

(defmacro on-planet (planet orbital-ratio)
  (let ((on-name (intern (concatenate 'string "ON-" (symbol-name planet)))))
    `(progn (defun ,on-name (seconds)
              (/ seconds (* earth-seconds ,orbital-ratio)))
            (export ',on-name))))

(on-planet earth   1)
(on-planet mercury 0.2408467)
(on-planet venus   0.61519726)
(on-planet mars    1.8808158)
(on-planet jupiter 11.862615)
(on-planet saturn  29.447498)
(on-planet uranus  84.016846)
(on-planet neptune 164.79132)
