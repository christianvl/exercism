# Instructions

The classical introductory exercise. Just say `"Hello, World!"`.

"Hello, World!" is the traditional first program for beginning programming in a new language or environment.

The objectives are simple:

    - Modify the provided code so that it produces the string "Hello, World!".
    - Run the test suite and make sure that it succeeds.
    - Submit your solution and check it at the website.

If everything goes well, you will be ready to fetch your first real exercise.

# How to debug

To help with debugging, you can use `print` or similar functions to write to standard output which will be visible in the test output.

For example adding `(print "hello there")` into the function you are writing will cause it to be visible in the test runs (given that the test executes that function).

One "trick": if you have several things to print out is to collect them together in a list and then print that: `(print (list :this "that" 'the-other))`.

