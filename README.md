[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](https://develop.spacemacs.org)

# Exercism

Solutions to exercises in [Exercism](https://www.exercism.org)

## Contributions
Contributions are welcome. Share your experience :).

## License
This software is licensed under the [GPL3](https://www.gnu.org/licenses/gpl-3.0.html) license.
![GPL](https://www.gnu.org/graphics/gplv3-88x31.png "GPL3")
