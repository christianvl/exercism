# Instructions

An anagram is a rearrangement of letters to form a new word. Given a word and a list of candidates, select the sublist of anagrams of the given word.

Given `"listen"` and a list of candidates like `"enlists" "google" "inlets" "banana"` the program should return a list containing `"inlets"`.

The instructions above refer to "lists" in the general sense and not to the `list()` data type in R. Note that the tests for this exercise in R expect a character vector i.e. `c()` to be returned and not a `list()`.
