score <- function(x, y) {
  r <- sqrt(x^2 + y^2)
  if (r > 10) 0
  else if (r <=10 & r > 5) 1
  else if (r <=5 & r > 1) 5
  else 10
}
